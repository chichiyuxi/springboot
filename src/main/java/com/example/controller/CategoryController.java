package com.example.controller;


import com.example.entity.Category;
import com.example.service.ICategoryService;
import com.example.utils.Result;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * <p>
 * 商品分类 前端控制器
 * </p>
 *
 * @author cyz
 * @since 2022-06-12
 */
@RestController
@RequestMapping("/category")
@CrossOrigin
@Api(value = "分类管理",tags = "分类管理")
public class CategoryController {

    @Autowired
    private ICategoryService categoryService;

    @GetMapping
    @ApiOperation(value = "商品分类列表")
    public Result listCategories(){
        return categoryService.listCategories();
    }

    @GetMapping("/{categoryId}")
    @ApiOperation(value = "获取单个商品分类信息")
    public Result getOne(@PathVariable Integer categoryId){
        return categoryService.getByInfoId(categoryId);
    }

    @GetMapping("/product/{categoryId}")
    @ApiOperation(value = "获取单个商品分类下商品")
    public Result getProducts(@PathVariable Integer categoryId){
        return categoryService.getProducts(categoryId);
    }

    @PostMapping
    @ApiOperation(value = "新增商品分类",notes ="id为自增")
    public Result saveCategory(@RequestBody Category category){
        return categoryService.saveCategory(category);
    }

    @DeleteMapping("/{categoryId}")
    @ApiOperation(value = "删除单个商品分类信息")
    public Result delete(@PathVariable Integer categoryId){
        return categoryService.delete(categoryId);
    }

    @PutMapping
    @ApiOperation(value = "修改单个商品分类信息")
    public Result update(@RequestBody Category category){
        return Result.success(categoryService.updateById(category));
    }










}

