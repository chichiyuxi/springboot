package com.example.controller;


import com.example.service.ICollectService;
import com.example.utils.Result;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author cyz
 * @since 2022-06-19
 */
@RestController
@RequestMapping("/collect")
@CrossOrigin
@Api(value = "收藏管理",tags = "收藏管理")
public class CollectController {

    @Autowired
    private ICollectService collectService;

    @PostMapping("/{productId}")
    @ApiOperation(value = "用户添加收藏")
    public Result saveCollect(@RequestHeader("Authorization") String token, @PathVariable Integer productId){
        return collectService.saveCollect(token,productId);
    }

    @GetMapping("/{userId}")
    @ApiOperation(value = "用户收藏列表")
    public Result getListByUser( @PathVariable Integer userId){
        return collectService.getListByUser(userId);
    }


    @DeleteMapping("/{collectId}")
    @ApiOperation(value = "用户删除收藏商品")
    public Result deleteCollect(@PathVariable Integer collectId){
        return Result.success(collectService.removeById(collectId));
    }




}

