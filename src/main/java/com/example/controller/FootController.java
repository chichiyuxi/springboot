package com.example.controller;


import com.example.service.IFootService;
import com.example.utils.Result;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author cyz
 * @since 2022-06-19
 */
@RestController
@RequestMapping("/foot")
@CrossOrigin
@Api(value = "足迹管理",tags = "足迹管理")
public class FootController {

    @Autowired
    private IFootService footService;

    @PostMapping("/{productId}")
    @ApiOperation(value = "用户浏览足迹")
    public Result saveFoot(@RequestHeader("Authorization") String token, @PathVariable Integer productId){
        return footService.saveFoot(token,productId);
    }

    @GetMapping("/{userId}")
    @ApiOperation(value = "用户足迹列表")
    public Result getListByUser( @PathVariable Integer userId){
        return footService.getListByUser(userId);
    }

    @DeleteMapping("/{footId}")
    @ApiOperation(value = "用户删除足迹信息")
    public Result deleteFoot( @PathVariable Integer footId){
        return Result.success(footService.removeById(footId));
    }

}

