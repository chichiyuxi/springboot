package com.example.controller;

import com.example.entity.IndexImg;
import com.example.service.IIndexImgService;
import com.example.utils.Result;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * <p>
 *  前端控制器
 * </p>
 * @author cyz
 * @since 2022-06-11
 */
@RestController
@RequestMapping("/banner")
@CrossOrigin
@Api(value = "轮播图管理",tags = "轮播图管理")
public class IndexImgController {

    @Autowired
    private IIndexImgService iIndexImgService;

    @GetMapping
    @ApiOperation(value = "获取轮播图列表")
    public Result getBanner(){
        return iIndexImgService.getBanner();
    }

    @PostMapping
    @ApiOperation(value = "添加轮播图",notes ="id为自增")
    public Result saveBanner(@RequestBody IndexImg indexImg){
        return Result.success(iIndexImgService.save(indexImg));
    }

    @GetMapping("/{imgId}")
    @ApiOperation(value = "获取单条轮播图信息")
    public Result getOne(@PathVariable Integer imgId){
        return Result.success(iIndexImgService.getById(imgId));
    }

    @PutMapping
    @ApiOperation(value = "修改轮播图信息")
    public Result update(@RequestBody IndexImg indexImg){
        return Result.success(iIndexImgService.updateById(indexImg));
    }

    @DeleteMapping("/{imgId}")
    @ApiOperation(value = "删除轮播图信息")
    public Result delete(@PathVariable Integer imgId){
        return Result.success(iIndexImgService.removeById(imgId));
    }


}

