package com.example.controller;


import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 * 订单项/快照  前端控制器
 * </p>
 *
 * @author cyz
 * @since 2022-06-20
 */
@RestController
@RequestMapping("/order-item")
public class OrderItemController {

}

