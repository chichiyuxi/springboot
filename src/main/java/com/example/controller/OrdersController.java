package com.example.controller;


import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 * 订单  前端控制器
 * </p>
 *
 * @author cyz
 * @since 2022-06-20
 */
@RestController
@RequestMapping("/orders")
@CrossOrigin
@Api(value = "订单管理",tags = "订单管理")
public class OrdersController {

//    @Autowired
//    private

}

