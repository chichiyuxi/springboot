package com.example.controller;


import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.example.service.IProductCommentsService;
import com.example.utils.Result;
import com.example.vo.ProductCommentsVO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * <p>
 * 商品评价  前端控制器
 * </p>
 *
 * @author cyz
 * @since 2022-06-16
 */
@RestController
@RequestMapping("/comments")
@CrossOrigin
@Api(value = "评论管理",tags = "评论管理")
public class ProductCommentsController {

    @Autowired
    private IProductCommentsService productCommentsService;

    @GetMapping("/{productId}")
    @ApiOperation(value = "获取单个商品评论信息")
    public Result getByList(@PathVariable Integer productId){
        return productCommentsService.listCommontsByProductId(productId);
    }

//    @GetMapping
//    @ApiOperation(value = "获取单个商品afcdsd评论信息")
//    public Result getByList(@RequestParam Integer pageNum, @RequestParam Integer pageSize){
//        return productCommentsService.listByComment(pageNum,pageSize);
//    }



}

