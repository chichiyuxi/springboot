package com.example.controller;

import com.example.service.IProductService;
import com.example.utils.Result;
import io.swagger.annotations.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * <p>
 * 商品前端控制器
 * </p>
 * @author cyz
 * @since 2022-06-13
 */
@RestController
@RequestMapping("/product")
@CrossOrigin
@Api(value = "商品管理接口",tags = "商品管理")
public class ProductController {

    @Autowired
    private IProductService productService;

    @GetMapping
    @ApiOperation(value = "商品推荐列表及搜索列表")
    @ApiImplicitParams({
            @ApiImplicitParam(dataType = "int",name = "pageNum", value = "第几页开始",required = true),
            @ApiImplicitParam(dataType = "int",name = "pageSize", value = "每页多少条记录",required = true),
            @ApiImplicitParam(dataType = "string",name = "productName", value = "要搜索的商品名称")
    })
    public Result getProList(@RequestParam Integer pageNum, @RequestParam Integer pageSize, @RequestParam(  defaultValue = "") String productName){
        return productService.getProList(pageNum,pageSize,productName);
    }

    @GetMapping("/{productId}")
    @ApiOperation(value = "获取单个商品基本信息")
    @ApiImplicitParam(dataType = "int",name = "productId", value = "商品id",required = true)
    public Result getOne(@PathVariable Integer productId){
        return productService.getInfoById(productId);
    }

//    @GetMapping("/hot")

}

