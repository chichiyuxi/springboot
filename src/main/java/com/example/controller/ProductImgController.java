package com.example.controller;


import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 * 商品图片  前端控制器
 * </p>
 *
 * @author cyz
 * @since 2022-06-13
 */
@RestController
@RequestMapping("/product-img")
public class ProductImgController {

}

