package com.example.controller;


import com.example.entity.ShoppingCart;
import com.example.service.IShoppingCartService;
import com.example.utils.Result;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * <p>
 * 购物车  前端控制器
 * </p>
 *
 * @author cyz
 * @since 2022-06-16
 */
@RestController
@RequestMapping("/cart")
@CrossOrigin
@Api(value = "购物车管理",tags = "购物车管理")
public class ShoppingCartController {

    @Autowired
    private IShoppingCartService shoppingCartService;

    @PostMapping
    @ApiOperation(value = "添加购物车")
    public Result  addCart(@RequestBody ShoppingCart shoppingCart,@RequestHeader("Authorization") String token){
        return shoppingCartService.addShoppingCart(shoppingCart,token);
    }

    @GetMapping("/{userId}")
    @ApiOperation(value = "用户购物车列表")
    public Result getListByUser(@PathVariable Integer userId){
        return shoppingCartService.getListByUser(userId);
    }

    @DeleteMapping("/{cartId}")
    @ApiOperation(value = "删除购物车")
    public Result deleteById(@PathVariable Integer cartId){
        return Result.success(shoppingCartService.removeById(cartId));
    }




}

