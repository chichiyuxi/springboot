package com.example.controller;


import com.example.entity.UserAddress;
import com.example.service.IUserAddressService;
import com.example.utils.Result;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author cyz
 * @since 2022-06-11
 */
@RestController
@RequestMapping("/address")
@CrossOrigin
@Api(value = "地址管理",tags = "地址管理")
public class UserAddressController {

    @Autowired
    private IUserAddressService userAddressService;

    @GetMapping
    @ApiOperation(value = "用户获取地址信息列表")
    public Result getAddress(@RequestHeader("Authorization") String token){
        return userAddressService.getAddress(token);
    }

    @GetMapping("/{addressId}")
    @ApiOperation(value = "用户获取单条地址信息")
    public Result getPerson(@PathVariable Integer addressId){
        return Result.success(userAddressService.getById(addressId));
    }

    @PostMapping
    @ApiOperation(value = "用户添加地址信息")
    public Result saveAddress(@RequestHeader("Authorization") String token, @RequestBody UserAddress userAddress){
        return userAddressService.saveAddress(token,userAddress);
    }

    @DeleteMapping("/{addressId}")
    @ApiOperation(value = "用户添加地址信息")
    public Result deleteAddress(@PathVariable Integer addressId){
        return Result.success(userAddressService.removeById(addressId));
    }
}

