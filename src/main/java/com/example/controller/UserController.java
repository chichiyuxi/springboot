package com.example.controller;

import com.example.entity.User;
import com.example.service.IUserService;
import com.example.utils.Result;
import com.example.vo.LoginVo;
import com.example.vo.RegisterVo;
import com.example.vo.ResetVo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * <p>
 *  用户控制器
 * </p>
 * @author cyz
 * @since 2022-06-11
 */
@RestController
@RequestMapping("/user")
@CrossOrigin
@Api(value = "用户管理接口",tags = "用户管理")
public class UserController {

    @Autowired
    private IUserService iUserService;

    /**
     * 用户注册
     */
    @PostMapping
    @ApiOperation(value = "用户注册")
    public Result register(@RequestBody RegisterVo registerVo){
        return iUserService.register(registerVo);
    }

    /**
     * 用户登录
     */
    @PostMapping("/login")
    @ApiOperation(value = "用户登录")
    public Result login(@RequestBody LoginVo loginVo){
         return iUserService.login(loginVo);
    }
    /**
     * 修改密码
     */
    @PutMapping("/reset")
    @ApiOperation(value = "修改密码")
    public Result reset(@RequestBody ResetVo resetVo){
        return iUserService.reset(resetVo);
    }
    /**
     * 用户信息
     */
    @GetMapping("/info")
    @ApiOperation(value = "获取用户信息")
    public Result info(@RequestHeader("Authorization") String token){
        return iUserService.info(token);
    }
    /**
     * 单个用户信息
     */
    @GetMapping("/info/{userId}")
    @ApiOperation(value = "查询单个用户信息")
    public Result getOne(@PathVariable Integer userId){
        return Result.success(iUserService.getById(userId));
    }
    /**
     * 修改用户信息
     */
    @PutMapping("/updateInfo")
    @ApiOperation(value = "修改用户信息")
    public Result update(@RequestBody User user){
        return Result.success(iUserService.updateById(user));
    }
    /**
     * 删除用户
     */
    @DeleteMapping("/{userId}")
    @ApiOperation(value = "删除用户")
    public Result delete(@PathVariable Integer userId){
        return Result.success(iUserService.removeById(userId));
    }
    /**
     * 用户列表
     */
    @GetMapping("/page")
    @ApiOperation(value = "用户列表分页和模糊查询", notes = "提交所需参数 pageNum=>当前页码 accountPwd=>每页显示条数 userName=>根据账号搜索 演示json参数是否接收成功")
    public Result page(@RequestParam Integer pageNum, @RequestParam Integer pageSize, @RequestParam(  defaultValue = "") String userName){
        return iUserService.getlist(pageNum,pageSize,userName);
    }

}

