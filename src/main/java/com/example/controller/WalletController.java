package com.example.controller;

import com.example.service.IWalletService;
import com.example.utils.Result;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * <p>
 *  前端控制器
 * </p>
 * @author cyz
 * @since 2022-06-19
 */
@RestController
@RequestMapping("/wallet")
@CrossOrigin
@Api(value = "钱包管理接口",tags = "钱包管理")
public class WalletController {

      @Autowired
      private IWalletService walletService;

      @PostMapping("/{paw}")
      @ApiOperation(value = "钱包认证")
      public Result register(@RequestHeader("Authorization") String token,@PathVariable String paw){
            return walletService.register(token,paw);
      }

      @PostMapping("/pay")
      @ApiOperation(value = "钱包充值")
      public Result pay(@RequestHeader("Authorization") String token,@RequestParam String paw,@RequestParam Integer money, @RequestParam(defaultValue = "0") Integer give){
            return walletService.pay(token,paw,money,give);
      }

      @GetMapping("/info")
      @ApiOperation(value = "用户获取钱包信息")
      public Result pay(@RequestHeader("Authorization") String token){
            return walletService.info(token);
      }


}
