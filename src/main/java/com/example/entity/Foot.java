package com.example.entity;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;
import java.time.LocalDateTime;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

/**
 * <p>
 * 
 * </p>
 *
 * @author cyz
 * @since 2022-06-19
 */
@Getter
@Setter
  @ApiModel(value = "Foot对象", description = "")
public class Foot implements Serializable {

    private static final long serialVersionUID = 1L;

      @ApiModelProperty("足迹主键id")
        @TableId(value = "id", type = IdType.AUTO)
      private Integer FootId;

      @ApiModelProperty("用户id")
      private Integer userId;

      @ApiModelProperty("商品id")
      private Integer productId;

      @ApiModelProperty("添加时间")
      @TableField(fill = FieldFill.INSERT)
      private LocalDateTime createTime;

      @ApiModelProperty("修改时间")
      @TableField(fill = FieldFill.INSERT_UPDATE)
      private LocalDateTime updateTime;


}
