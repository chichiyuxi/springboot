package com.example.entity;

import com.baomidou.mybatisplus.annotation.*;

import java.io.Serializable;
import java.time.LocalDateTime;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

/**
 * <p>
 * 内容轮播图管理
 * </p>
 *
 * @author cyz
 * @since 2022-06-11
 */
@Getter
@Setter
  @TableName("index_img")
@ApiModel(value = "IndexImg对象", description = "")
public class IndexImg implements Serializable {

    private static final long serialVersionUID = 1L;

      @ApiModelProperty("轮播图主键id")
      @TableId(value = "id", type = IdType.AUTO)
      private Integer id;

      @ApiModelProperty("图片地址")
      private String imgUrl;

      @ApiModelProperty("商品id")
      private Integer prodId;

      @ApiModelProperty("商品分类id")
      private Integer categoryId;

      @ApiModelProperty("轮播图类型，用于判断，可以根据商品id或者分类进行页面跳转，1：商品 2：分类")
      private Integer indexType;

      @ApiModelProperty("轮播图展示顺序 轮播图展示顺序，从小到大")
      private Integer sort;

      @ApiModelProperty("是否展示:1表示正常显示，0表示下线 是否展示，1：展示    0：不展示")
      private Integer status;

      @ApiModelProperty("创建时间")
      @TableField(fill = FieldFill.INSERT)
      private LocalDateTime createTime;

      @ApiModelProperty("更新时间")
      @TableField(fill = FieldFill.INSERT_UPDATE)
      private LocalDateTime updateTime;


}
