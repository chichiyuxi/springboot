package com.example.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

/**
 * <p>
 * 订单 
 * </p>
 *
 * @author cyz
 * @since 2022-06-20
 */
@Getter
@Setter
  @ApiModel(value = "Orders对象", description = "订单 ")
public class Orders implements Serializable {

    private static final long serialVersionUID = 1L;

      @ApiModelProperty("订单ID 同时也是订单编号")
        private String orderId;

      @ApiModelProperty("用户ID")
      private Integer userId;

      @ApiModelProperty("产品名称（多个产品用,隔开）")
      private String untitled;

      @ApiModelProperty("收货人快照")
      private String receiverName;

      @ApiModelProperty("收货人手机号快照")
      private String receiverMobile;

      @ApiModelProperty("收货地址快照")
      private String receiverAddress;

      @ApiModelProperty("订单总价格")
      private BigDecimal totalAmount;

      @ApiModelProperty("实际支付总价格")
      private Integer actualAmount;

      @ApiModelProperty("支付方式 1:微信 2:支付宝")
      private Integer payType;

      @ApiModelProperty("订单备注")
      private String orderRemark;

      @ApiModelProperty("订单状态 1:待付款 2:待发货 3:待收货 4:待评价 5:已完成 6:已关闭")
      private String status;

      @ApiModelProperty("配送方式")
      private String deliveryType;

      @ApiModelProperty("物流单号")
      private String deliveryFlowId;

      @ApiModelProperty("订单运费 默认可以为零，代表包邮")
      private BigDecimal orderFreight;

      @ApiModelProperty("逻辑删除状态 1: 删除 0:未删除")
      private Integer deleteStatus;

      @ApiModelProperty("创建时间（成交时间）")
      private LocalDateTime createTime;

      @ApiModelProperty("更新时间")
      private LocalDateTime updateTime;

      @ApiModelProperty("付款时间")
      private LocalDateTime payTime;

      @ApiModelProperty("发货时间")
      private LocalDateTime deliveryTime;

      @ApiModelProperty("完成时间")
      private LocalDateTime flishTime;

      @ApiModelProperty("取消时间")
      private LocalDateTime cancelTime;

      @ApiModelProperty("订单关闭类型1-超时未支付 2-退款关闭 4-买家取消 15-已通过货到付款交易")
      private Integer closeType;


}
