package com.example.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;
import java.time.LocalDateTime;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

/**
 * <p>
 * 商品参数表
 * </p>
 *
 * @author cyz
 * @since 2022-06-13
 */
@Getter
@Setter
  @ApiModel(value = "Product对象", description = "商品 商品信息相关表：分类表，商品图片表，商品规格表，商品参数表")
public class Product implements Serializable {

    private static final long serialVersionUID = 1L;

      @ApiModelProperty("商品主键id")
      @TableId(value = "product_id", type = IdType.AUTO)
      private Integer productId;

      @ApiModelProperty("商品名称 商品名称")
      private String productName;

      @ApiModelProperty("分类外键id 分类id")
      private Integer categoryId;

      @ApiModelProperty("销量 累计销售")
      private Integer soldNum;

      @ApiModelProperty("默认是1，表示正常状态, -1表示删除, 0下架 默认是1，表示正常状态, -1表示删除, 0下架")
      private Integer productStatus;

      @ApiModelProperty("商品主图")
      private String mainPic;

      @ApiModelProperty("商品内容 商品内容")
      private String content;

      @ApiModelProperty("创建时间")
      private LocalDateTime createTime;

      @ApiModelProperty("更新时间")
      private LocalDateTime updateTime;


}
