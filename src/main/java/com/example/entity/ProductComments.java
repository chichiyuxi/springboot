package com.example.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;
import java.time.LocalDateTime;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

/**
 * <p>
 * 商品评价 
 * </p>
 *
 * @author cyz
 * @since 2022-06-16
 */
@Getter
@Setter
  @TableName("product_comments")
@ApiModel(value = "ProductComments对象", description = "商品评价 ")
public class ProductComments implements Serializable {

    private static final long serialVersionUID = 1L;

      @ApiModelProperty("ID")
        @TableId(value = "comm_id", type = IdType.AUTO)
      private Integer commId;

      @ApiModelProperty("商品id")
      private Integer productId;

      @ApiModelProperty("商品名称")
      private String productName;

      @ApiModelProperty("订单项(商品快照)ID 可为空")
      private Integer orderItemId;

        @ApiModelProperty("评论用户id 用户名须脱敏")
        private Integer userId;

      @ApiModelProperty("是否匿名（1:是，0:否）")
      private Integer isAnonymous;

      @ApiModelProperty("评价类型（1好评，0中评，-1差评）")
      private Integer commType;

      @ApiModelProperty("评价等级 1：好评 2：中评 3：差评")
      private Integer commLevel;

      @ApiModelProperty("评价内容")
      private String commContent;

      @ApiModelProperty("评价晒图(JSON {img1:url1,img2:url2} )")
      private String commImgs;

      @ApiModelProperty("评价时间 可为空")
      private LocalDateTime createTime;

      @ApiModelProperty("是否回复（0:未回复，1:已回复）")
      private Integer replyStatus;

      @ApiModelProperty("回复内容")
      private String replyContent;

      @ApiModelProperty("回复时间")
      private LocalDateTime replyTime;

      @ApiModelProperty("是否显示（1:是，0:否）")
      private Integer isShow;


}
