package com.example.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;
import java.time.LocalDateTime;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import jdk.nashorn.internal.ir.annotations.Ignore;
import lombok.Getter;
import lombok.Setter;

/**
 * <p>
 * 商品图片 
 * </p>
 *
 * @author cyz
 * @since 2022-06-13
 */
@Getter
@Setter
  @TableName("product_img")
@ApiModel(value = "ProductImg对象", description = "商品图片 ")
public class ProductImg implements Serializable {

    private static final long serialVersionUID = 1L;

      @ApiModelProperty("图片主键")
        @TableId(value = "id", type = IdType.AUTO)
      private Integer id;

      @ApiModelProperty("商品外键id 商品外键id")
      private Integer itemId;

      @ApiModelProperty("图片地址 图片地址")
      private String url;

      @ApiModelProperty("顺序 图片顺序，从小到大")
      private Integer sort;

//      @ApiModelProperty("创建时间")
//      @TableField(exist = false)
//      private LocalDateTime createdTime;

//      @ApiModelProperty("更新时间")
//      @TableField(exist = false)
//      private LocalDateTime updatedTime;


}
