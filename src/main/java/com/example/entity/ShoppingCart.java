package com.example.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;
import java.math.BigDecimal;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

/**
 * <p>
 * 购物车 
 * </p>
 *
 * @author cyz
 * @since 2022-06-16
 */
@Getter
@Setter
  @TableName("shopping_cart")
@ApiModel(value = "ShoppingCart对象", description = "购物车 ")
public class ShoppingCart implements Serializable {

    private static final long serialVersionUID = 1L;

      @ApiModelProperty("主键")
        @TableId(value = "cart_id", type = IdType.AUTO)
      private Integer cartId;

      @ApiModelProperty("商品ID")
      private Integer productId;

      @ApiModelProperty("skuID")
      private Integer skuId;

      @ApiModelProperty("用户ID")
      private Integer userId;

      @ApiModelProperty("购物车商品数量")
      private Integer cartNum;

      @ApiModelProperty("添加购物车时间")
      private String cartTime;

      @ApiModelProperty("添加购物车时商品价格")
      private BigDecimal productPrice;

      @ApiModelProperty("选择的套餐的属性")
      private String skuProps;


}
