package com.example.entity;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;
import java.time.LocalDate;
import java.time.LocalDateTime;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

/**
 * <p>
 * 用户管理
 * </p>
 *
 * @author cyz
 * @since 2022-06-11
 */
@Getter
@Setter
  @ApiModel(value = "User对象", description = "")
public class User implements Serializable {

    private static final long serialVersionUID = 1L;

      @ApiModelProperty("用户主键id")
      @TableId(value = "id", type = IdType.AUTO)
      private Integer id;

      @ApiModelProperty("用户名")
      private String username;

      @ApiModelProperty("密码")
      private String password;

      @ApiModelProperty("昵称")
      private String nickname;

      @ApiModelProperty("性别 1(男) or 0(女)")
      private Integer userSex;

      @ApiModelProperty("生日")
      private LocalDate userBirth;

      @ApiModelProperty("真实姓名")
      private String realname;

      @ApiModelProperty("头像")
      private String userImg;

      @ApiModelProperty("手机号")
      private Integer userMobile;

      @ApiModelProperty("邮箱地址")
      private String userEmail;

      @ApiModelProperty("角色 管理员 1 or 普通用户 0")
      private Integer role;

      @ApiModelProperty("注册时间")
      @TableField(fill = FieldFill.INSERT)
      private LocalDateTime createTime;

      @ApiModelProperty("更新时间")
      @TableField(fill = FieldFill.INSERT_UPDATE)
      private LocalDateTime updateTime;


}
