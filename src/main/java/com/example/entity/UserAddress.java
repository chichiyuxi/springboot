package com.example.entity;

import com.baomidou.mybatisplus.annotation.*;

import java.io.Serializable;
import java.time.LocalDateTime;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

/**
 * <p>
 * 用户地址管理
 * </p>
 *
 * @author cyz
 * @since 2022-06-11
 */
@Getter
@Setter
  @TableName("user_address")
@ApiModel(value = "UserAddress对象", description = "")
public class UserAddress implements Serializable {

    private static final long serialVersionUID = 1L;

      @ApiModelProperty("地址主键id")
        @TableId(value = "id", type = IdType.AUTO)
      private Integer id;

      @ApiModelProperty("用户ID")
      private Integer userId;

      @ApiModelProperty("收件人姓名")
      private String receiverName;

      @ApiModelProperty("收件人电话")
      private String receiverMobile;

      @ApiModelProperty("省份")
      private String province;

      @ApiModelProperty("城市")
      private String city;

      @ApiModelProperty("区县")
      private String area;

      @ApiModelProperty("详细地址")
      private String address;

      @ApiModelProperty("邮编")
      private String postCode;

      @ApiModelProperty("状态,1正常，0无效")
      private Integer status;

      @ApiModelProperty("是否默认地址 1是 1:是  0:否")
      private Integer commonAddr;

      @ApiModelProperty("创建时间")
      @TableField(fill = FieldFill.INSERT)
      private LocalDateTime createTime;

      @ApiModelProperty("更新时间")
      @TableField(fill = FieldFill.INSERT_UPDATE)
      private LocalDateTime updateTime;


}
