package com.example.entity;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;
import java.time.LocalDateTime;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

/**
 * <p>
 * </p>
 *
 * @author cyz
 * @since 2022-06-19
 */
@Getter
@Setter
@ApiModel(value = "Wallet对象", description = "")
public class Wallet implements Serializable {

    private static final long serialVersionUID = 1L;

      @ApiModelProperty("钱包主键id")
      @TableId(value = "id", type = IdType.AUTO)
      private Integer id;

      @ApiModelProperty("用户id")
      private Integer userId;

      @ApiModelProperty("钱包密码")
      private String walletPassword;

      @ApiModelProperty("钱包余额")
      private Integer sum;

      @ApiModelProperty("累计充值")
      private Integer total;

      @ApiModelProperty("累计消费")
      private Integer sumOut;

      @ApiModelProperty("积分")
      private Integer score;

      @ApiModelProperty("创建时间")
      @TableField(fill = FieldFill.INSERT)
      private LocalDateTime createTime;

      @ApiModelProperty("更新时间")
      @TableField(fill = FieldFill.INSERT_UPDATE)
      private LocalDateTime updateTime;


}
