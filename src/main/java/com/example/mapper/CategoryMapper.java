package com.example.mapper;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.example.entity.Category;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.example.vo.CategoryVo;
import com.example.vo.ProductVo;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * <p>
 * 商品分类 Mapper 接口
 * </p>
 *
 * @author cyz
 * @since 2022-06-12
 */
@Mapper
public interface CategoryMapper extends BaseMapper<Category> {

    //1.连接查询
    public List<CategoryVo> selectAllCategories();

    //2.子查询：根据parentId查询子分类
    public List<CategoryVo> selectAllCategories2(int parentId);

    List<CategoryVo> selectAllCategories3(int parentId);


}
