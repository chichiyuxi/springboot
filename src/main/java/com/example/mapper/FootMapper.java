package com.example.mapper;

import com.example.entity.Foot;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author cyz
 * @since 2022-06-19
 */
@Mapper
public interface FootMapper extends BaseMapper<Foot> {

}
