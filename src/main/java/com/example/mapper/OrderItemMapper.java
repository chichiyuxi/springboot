package com.example.mapper;

import com.example.entity.OrderItem;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * <p>
 * 订单项/快照  Mapper 接口
 * </p>
 *
 * @author cyz
 * @since 2022-06-20
 */
@Mapper
public interface OrderItemMapper extends BaseMapper<OrderItem> {

}
