package com.example.mapper;

import com.example.entity.Orders;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * <p>
 * 订单  Mapper 接口
 * </p>
 *
 * @author cyz
 * @since 2022-06-20
 */
@Mapper
public interface OrdersMapper extends BaseMapper<Orders> {

}
