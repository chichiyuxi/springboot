package com.example.mapper;

import com.example.entity.ProductComments;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.example.vo.ProductCommentsVO;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * <p>
 * 商品评价  Mapper 接口
 * </p>
 * @author cyz
 * @since 2022-06-16
 */
@Mapper
public interface ProductCommentsMapper extends BaseMapper<ProductComments> {

    public List<ProductCommentsVO> selectCommontsByProductId(Integer productId);

}
