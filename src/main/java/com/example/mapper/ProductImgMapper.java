package com.example.mapper;

import com.example.entity.ProductImg;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * <p>
 * 商品图片  Mapper 接口
 * </p>
 *
 * @author cyz
 * @since 2022-06-13
 */
@Mapper
public interface ProductImgMapper extends BaseMapper<ProductImg> {

}
