package com.example.mapper;

import com.example.entity.ProductParams;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * <p>
 * 商品参数  Mapper 接口
 * </p>
 *
 * @author cyz
 * @since 2022-06-16
 */
@Mapper
public interface ProductParamsMapper extends BaseMapper<ProductParams> {

}
