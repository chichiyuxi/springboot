package com.example.mapper;

import com.example.entity.ProductSku;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.example.vo.ShoppingCartVo;
import org.apache.ibatis.annotations.Mapper;

/**
 * <p>
 * 商品规格 每一件商品都有不同的规格，不同的规格又有不同的价格和优惠力度，规格表为此设计 Mapper 接口
 * </p>
 *
 * @author cyz
 * @since 2022-06-15
 */
@Mapper
public interface ProductSkuMapper extends BaseMapper<ProductSku> {

}
