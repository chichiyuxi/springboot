package com.example.mapper;

import com.example.entity.ShoppingCart;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.example.vo.ShoppingCartVo;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * <p>
 * 购物车  Mapper 接口
 * </p>
 *
 * @author cyz
 * @since 2022-06-16
 */
@Mapper
public interface ShoppingCartMapper extends BaseMapper<ShoppingCart> {
    public List<ShoppingCartVo> selectShopcartByUserId(int userId);


}
