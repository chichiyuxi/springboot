package com.example.mapper;

import com.example.entity.UserAddress;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author cyz
 * @since 2022-06-11
 */
@Mapper
public interface UserAddressMapper extends BaseMapper<UserAddress> {

}
