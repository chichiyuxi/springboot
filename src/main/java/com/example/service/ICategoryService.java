package com.example.service;

import com.example.entity.Category;
import com.baomidou.mybatisplus.extension.service.IService;
import com.example.utils.Result;

/**
 * <p>
 * 商品分类 服务类
 * </p>
 *
 * @author cyz
 * @since 2022-06-12
 */
public interface ICategoryService extends IService<Category> {
       Result listCategories();

       Result saveCategory(Category category);

       Result delete(Integer categoryId);

       Result getByInfoId(Integer categoryId);

       Result getProducts(Integer categoryId);

}
