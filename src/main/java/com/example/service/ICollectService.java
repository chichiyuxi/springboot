package com.example.service;

import com.example.entity.Collect;
import com.baomidou.mybatisplus.extension.service.IService;
import com.example.utils.Result;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author cyz
 * @since 2022-06-19
 */
public interface ICollectService extends IService<Collect> {

    Result saveCollect(String token, Integer productId);

    Result getListByUser(Integer userId);

}
