package com.example.service;

import com.example.entity.IndexImg;
import com.baomidou.mybatisplus.extension.service.IService;
import com.example.utils.Result;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author cyz
 * @since 2022-06-11
 */
public interface IIndexImgService extends IService<IndexImg> {

    Result getBanner();

}
