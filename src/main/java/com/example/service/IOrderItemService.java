package com.example.service;

import com.example.entity.OrderItem;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 订单项/快照  服务类
 * </p>
 *
 * @author cyz
 * @since 2022-06-20
 */
public interface IOrderItemService extends IService<OrderItem> {

}
