package com.example.service;

import com.example.entity.Orders;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 订单  服务类
 * </p>
 *
 * @author cyz
 * @since 2022-06-20
 */
public interface IOrdersService extends IService<Orders> {

}
