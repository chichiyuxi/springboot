package com.example.service;

import com.example.entity.ProductComments;
import com.baomidou.mybatisplus.extension.service.IService;
import com.example.utils.Result;

import java.util.List;

/**
 * <p>
 * 商品评价  服务类
 * </p>
 *
 * @author cyz
 * @since 2022-06-16
 */
public interface IProductCommentsService extends IService<ProductComments> {

     Result listCommontsByProductId(Integer productId);

     Result listByComment(Integer pageNum, Integer pageSize);
}
