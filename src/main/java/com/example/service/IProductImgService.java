package com.example.service;

import com.example.entity.ProductImg;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 商品图片  服务类
 * </p>
 *
 * @author cyz
 * @since 2022-06-13
 */
public interface IProductImgService extends IService<ProductImg> {

}
