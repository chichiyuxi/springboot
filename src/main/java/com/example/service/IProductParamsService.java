package com.example.service;

import com.example.entity.ProductParams;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 商品参数  服务类
 * </p>
 *
 * @author cyz
 * @since 2022-06-16
 */
public interface IProductParamsService extends IService<ProductParams> {

}
