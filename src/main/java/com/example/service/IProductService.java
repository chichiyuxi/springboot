package com.example.service;

import com.example.entity.Product;
import com.baomidou.mybatisplus.extension.service.IService;
import com.example.utils.Result;

/**
 * <p>
 * 商品 商品信息相关表：分类表，商品图片表，商品规格表，商品参数表 服务类
 * </p>
 *
 * @author cyz
 * @since 2022-06-13
 */
public interface IProductService extends IService<Product> {

    Result getProList(Integer pageNum, Integer pageSize, String productName);

    Result getInfoById(Integer productId);

}
