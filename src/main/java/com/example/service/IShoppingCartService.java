package com.example.service;

import com.example.entity.ShoppingCart;
import com.baomidou.mybatisplus.extension.service.IService;
import com.example.utils.Result;

import java.util.List;

/**
 * <p>
 * 购物车  服务类
 * </p>
 *
 * @author cyz
 * @since 2022-06-16
 */
public interface IShoppingCartService extends IService<ShoppingCart> {
    Result addShoppingCart(ShoppingCart cart,String token);

    Result getListByUser (Integer userid);


}
