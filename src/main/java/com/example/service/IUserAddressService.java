package com.example.service;

import com.example.entity.UserAddress;
import com.baomidou.mybatisplus.extension.service.IService;
import com.example.utils.Result;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author cyz
 * @since 2022-06-11
 */
public interface IUserAddressService extends IService<UserAddress> {

    Result getAddress(String token);

    Result saveAddress(String token,UserAddress userAddress);

}
