package com.example.service;

import com.example.entity.User;
import com.baomidou.mybatisplus.extension.service.IService;
import com.example.utils.Result;
import com.example.vo.LoginVo;
import com.example.vo.RegisterVo;
import com.example.vo.ResetVo;
import org.springframework.web.multipart.MultipartFile;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author cyz
 * @since 2022-06-11
 */
public interface IUserService extends IService<User> {
    //用户注册
    Result register(RegisterVo registerVo);

    Result login(LoginVo loginVo);

    Result info(String token);

    User find(String username, String password);

    User check(String token);

    Result getlist(Integer pageNum, Integer pageSize, String userName);

    Result reset(ResetVo resetVo);


}
