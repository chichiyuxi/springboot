package com.example.service;

import com.example.entity.Wallet;
import com.baomidou.mybatisplus.extension.service.IService;
import com.example.utils.Result;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author cyz
 * @since 2022-06-19
 */
public interface IWalletService extends IService<Wallet> {

    Result register(String token, String paw);

    Result pay(String token,String paw, Integer money, Integer give);

    Result info(String token);

}
