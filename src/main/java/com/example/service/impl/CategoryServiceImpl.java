package com.example.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.api.R;
import com.example.entity.Category;
import com.example.entity.Product;
import com.example.mapper.CategoryMapper;
import com.example.mapper.ProductMapper;
import com.example.service.ICategoryService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.example.utils.Result;
import com.example.vo.CategoryVo;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.List;

/**
 * <p>
 * 商品分类 服务实现类
 * </p>
 *
 * @author cyz
 * @since 2022-06-12
 */
@Service
public class CategoryServiceImpl extends ServiceImpl<CategoryMapper, Category> implements ICategoryService {

    @Autowired
    private CategoryMapper categoryMapper;

    @Autowired
    private ProductMapper productMapper;

    @Override
    public Result listCategories() {
        List<CategoryVo> categoryVos = categoryMapper.selectAllCategories();
        return Result.success(categoryVos);
    }

    @Override
    public Result getByInfoId(Integer categoryId) {
        List<CategoryVo> categoryVos = categoryMapper.selectAllCategories3(categoryId);
        return Result.success(categoryVos);
    }

    @Override
    public Result getProducts(Integer categoryId) {
        LambdaQueryWrapper<Product> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(Product::getCategoryId,categoryId);
        List<Product> productList = productMapper.selectList(queryWrapper);
        return Result.success(productList);
    }

    @Override
    public Result saveCategory(Category category) {
        String categoryName = category.getCategoryName();
        Integer categoryLevel = category.getCategoryLevel();
        Integer parentId = category.getParentId();
        String categoryIcon = category.getCategoryIcon();
        String categorySlogan = category.getCategorySlogan();
        String categoryPic = category.getCategoryPic();
        if (StringUtils.isEmpty(categoryName)||StringUtils.isEmpty(categoryLevel)||StringUtils.isEmpty(parentId)
                ||StringUtils.isEmpty(categoryIcon)||StringUtils.isEmpty(categorySlogan)||StringUtils.isEmpty(categoryPic)){
            return Result.fail(400,"信息不能为空");
        }else {
            LambdaQueryWrapper<Category> queryWrapper = new LambdaQueryWrapper<>();
            queryWrapper.eq(Category::getCategoryName,categoryName);
            Category selectOne = categoryMapper.selectOne(queryWrapper);
            if (selectOne == null){
                Category categorySave = new Category();
                BeanUtils.copyProperties(category,categorySave);
                int insert = categoryMapper.insert(categorySave);
                return Result.success(insert);
            }else {
                return Result.fail(400,"分类名已存在");
            }

        }
    }

    @Override
    public Result delete(Integer categoryId) {
        LambdaQueryWrapper<Category> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(Category::getParentId,categoryId);
        List<Category> categories = categoryMapper.selectList(queryWrapper);
        if (categories.size()==0){
            int i = categoryMapper.deleteById(categoryId);
            return Result.success(i);
        }else {
            return Result.fail(400,"要删除的分类下有子级分类");
        }
    }




}
