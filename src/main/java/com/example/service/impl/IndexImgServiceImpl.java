package com.example.service.impl;

import com.example.entity.IndexImg;
import com.example.mapper.IndexImgMapper;
import com.example.service.IIndexImgService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.example.utils.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author cyz
 * @since 2022-06-11
 */
@Service
public class IndexImgServiceImpl extends ServiceImpl<IndexImgMapper, IndexImg> implements IIndexImgService {

    @Autowired
    private IndexImgMapper indexImgMapper;

    @Override
    public Result getBanner() {
        List<IndexImg> indexImgs = indexImgMapper.selectList(null);
        return Result.success(indexImgs);
    }
}
