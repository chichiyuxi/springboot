package com.example.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.example.entity.ProductComments;
import com.example.mapper.ProductCommentsMapper;
import com.example.service.IProductCommentsService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.example.utils.PageHelper;
import com.example.utils.Result;
import com.example.vo.ProductCommentsVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 * 商品评价  服务实现类
 * </p>
 *
 * @author cyz
 * @since 2022-06-16
 */
@Service
public class ProductCommentsServiceImpl extends ServiceImpl<ProductCommentsMapper, ProductComments> implements IProductCommentsService {

    @Autowired
    private ProductCommentsMapper productCommentsMapper;

    @Autowired
    private IProductCommentsService productCommentsService;


    @Override
    public Result listCommontsByProductId(Integer productId) {
        List<ProductCommentsVO> productCommentsVOS = productCommentsMapper.selectCommontsByProductId(productId);
        return Result.success(productCommentsVOS);
    }

    @Override
    public Result listByComment(Integer pageNum, Integer pageSize) {
        Page<ProductComments> page = productCommentsService.page(new Page<>(pageNum, pageSize), null);
        return Result.success(page);
    }

}
