package com.example.service.impl;

import com.example.entity.ProductImg;
import com.example.mapper.ProductImgMapper;
import com.example.service.IProductImgService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 商品图片  服务实现类
 * </p>
 *
 * @author cyz
 * @since 2022-06-13
 */
@Service
public class ProductImgServiceImpl extends ServiceImpl<ProductImgMapper, ProductImg> implements IProductImgService {

}
