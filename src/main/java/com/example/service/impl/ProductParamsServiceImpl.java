package com.example.service.impl;

import com.example.entity.ProductParams;
import com.example.mapper.ProductParamsMapper;
import com.example.service.IProductParamsService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 商品参数  服务实现类
 * </p>
 *
 * @author cyz
 * @since 2022-06-16
 */
@Service
public class ProductParamsServiceImpl extends ServiceImpl<ProductParamsMapper, ProductParams> implements IProductParamsService {

}
