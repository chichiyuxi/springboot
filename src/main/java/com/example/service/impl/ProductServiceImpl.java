package com.example.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.example.entity.Product;
import com.example.entity.ProductImg;
import com.example.entity.ProductParams;
import com.example.entity.ProductSku;
import com.example.mapper.ProductMapper;
import com.example.service.IProductImgService;
import com.example.service.IProductParamsService;
import com.example.service.IProductService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.example.service.IProductSkuService;
import com.example.utils.Result;
import com.example.vo.ProductVo;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.stream.Collectors;

/**
 * <p>
 * 商品服务实现类
 * </p>
 *
 * @author cyz
 * @since 2022-06-13
 */
@Service
public class ProductServiceImpl extends ServiceImpl<ProductMapper, Product> implements IProductService {

    @Autowired
    private IProductService productService;

    @Autowired
    private IProductImgService productImgService;

    @Autowired
    private IProductSkuService productSkuService;

    @Autowired
    private IProductParamsService productParamsService;

    @Override
    public Result getProList(Integer pageNum, Integer pageSize, String productName) {
        Page<Product> page = new Page<>(pageNum, pageSize);
        Page<ProductVo> objectPage = new Page<>();
        LambdaQueryWrapper<Product> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.like(Product::getProductName,productName);
        queryWrapper.orderByAsc(Product::getProductId);
        Page<Product> productPage = productService.page(page, queryWrapper);
        if (productPage.getRecords().size()==0){
            return Result.fail(400,"你搜索的商品不存在");
        }
        BeanUtils.copyProperties(page,objectPage,"records");
        List<Product> records = page.getRecords();
        List<ProductVo> productVoList = records.stream().map((item) -> {
            ProductVo productVo = new ProductVo();
            BeanUtils.copyProperties(item, productVo);
            Integer productId = item.getProductId();
            LambdaQueryWrapper<ProductImg> wrapper = new LambdaQueryWrapper<>();
            wrapper.eq(ProductImg::getItemId, productId);
            wrapper.orderByAsc(ProductImg::getSort);
            List<ProductImg> imgs = productImgService.list(wrapper);
            if (imgs != null) {
                productVo.setImgList(imgs);
            }
            return productVo;
        }).collect(Collectors.toList());
        Page<ProductVo> productVoPage = objectPage.setRecords(productVoList);
        return Result.success(productVoPage);
    }

    @Override
    public Result getInfoById(Integer productId) {
        LambdaQueryWrapper<Product> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(Product::getProductId,productId);
        List<Product> productList = productService.list(queryWrapper);
        if (productList.size()>0){
            LambdaQueryWrapper<ProductImg> queryWrapper1 = new LambdaQueryWrapper<>();
            queryWrapper1.eq(ProductImg::getItemId,productId);
            List<ProductImg> productImgList = productImgService.list(queryWrapper1);

            LambdaQueryWrapper<ProductSku> queryWrapper2 = new LambdaQueryWrapper<>();
            queryWrapper2.eq(ProductSku::getProductId,productId);
            queryWrapper2.eq(ProductSku::getStatus,1);
            List<ProductSku> productSkuList = productSkuService.list(queryWrapper2);

            LambdaQueryWrapper<ProductParams> queryWrapper3 = new LambdaQueryWrapper<>();
            queryWrapper3.eq(ProductParams::getProductId,productId);
            List<ProductParams> productParamsList = productParamsService.list(queryWrapper3);

            HashMap<String, Object> map = new HashMap<>();
            map.put("product",productList.get(0));
            map.put("productImg",productImgList);
            map.put("productSku",productSkuList);
            map.put("productParams",productParamsList);

            return Result.success(map);
        }else {
            return Result.fail(400,"商品不存在");

        }

    }
}
