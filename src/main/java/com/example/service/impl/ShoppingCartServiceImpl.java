package com.example.service.impl;

import com.example.entity.ShoppingCart;
import com.example.entity.User;
import com.example.mapper.ShoppingCartMapper;
import com.example.service.IShoppingCartService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.example.service.IUserService;
import com.example.utils.Result;
import com.example.vo.ShoppingCartVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * <p>
 * 购物车  服务实现类
 * </p>
 *
 * @author cyz
 * @since 2022-06-16
 */
@Service
public class ShoppingCartServiceImpl extends ServiceImpl<ShoppingCartMapper, ShoppingCart> implements IShoppingCartService {

    @Autowired
    private ShoppingCartMapper shoppingCartMapper;

    @Autowired
    private IUserService userService;

    @Override
    public Result addShoppingCart(ShoppingCart cart,String token) {
        User user = userService.check(token);
        if (user==null){
            return Result.fail(400,"token不存在");
        }
        cart.setUserId(user.getId());
        int i = shoppingCartMapper.insert(cart);
        if (i>0){
            return Result.success(1);
        }else {
            return Result.fail(400,"添加失败");
        }
    }

    @Override
    public Result getListByUser(Integer userid) {
        List<ShoppingCartVo> shoppingCartVos = shoppingCartMapper.selectShopcartByUserId(userid);
        return Result.success(shoppingCartVos);
    }


}
