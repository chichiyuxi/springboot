package com.example.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.example.entity.User;
import com.example.entity.UserAddress;
import com.example.mapper.UserAddressMapper;
import com.example.service.IUserAddressService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.example.service.IUserService;
import com.example.utils.Result;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author cyz
 * @since 2022-06-11
 */
@Service
public class UserAddressServiceImpl extends ServiceImpl<UserAddressMapper, UserAddress> implements IUserAddressService {

    @Autowired
    private IUserService  userService;

    @Autowired
    private IUserAddressService addressService;

    @Override
    public Result getAddress(String token) {
        User check = userService.check(token);
        Integer userId = check.getId();
        LambdaQueryWrapper<UserAddress> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(UserAddress::getUserId,userId);
        List<UserAddress> userAddressList = addressService.list(queryWrapper);
        return Result.success(userAddressList);
    }

    @Override
    public Result saveAddress(String token,UserAddress userAddress) {
        User check = userService.check(token);
        Integer userId = check.getId();
        userAddress.setUserId(userId);
        UserAddress saveAddress = new UserAddress();
        BeanUtils.copyProperties(userAddress,saveAddress);
        addressService.save(saveAddress);
        return Result.success(1);
    }
}
