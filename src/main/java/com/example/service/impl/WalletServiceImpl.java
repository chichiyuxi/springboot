package com.example.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.example.entity.User;
import com.example.entity.Wallet;
import com.example.mapper.WalletMapper;
import com.example.service.IUserService;
import com.example.service.IWalletService;
import com.example.utils.MD5Utils;
import com.example.utils.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

/**
 * <p>
 *  服务实现类
 * </p>
 * @author cyz
 * @since 2022-06-19
 */
@Service
public class WalletServiceImpl extends ServiceImpl<WalletMapper, Wallet> implements IWalletService {

    @Autowired
    private IUserService userService;

    @Autowired
    private WalletMapper walletMapper;

    @Autowired
    private IWalletService walletService;

    @Override
    public Result register(String token, String paw) {
        User check = userService.check(token);
        if (check==null){
            return Result.fail(400,"数据异常");
        }
        if (StringUtils.isEmpty(paw)){
            return Result.fail(400,"钱包密码不能为空");
        }
        LambdaQueryWrapper<Wallet> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(Wallet::getUserId,check.getId());
        Wallet wallet = walletMapper.selectOne(queryWrapper);
        if (wallet!=null){
            return  Result.fail(400,"钱包已认证");
        }
        String md5Pwd = MD5Utils.md5(paw);
        Wallet walletReg = new Wallet();
        walletReg.setUserId(check.getId());
        walletReg.setWalletPassword(md5Pwd);
        boolean save = walletService.save(walletReg);
        if (save){
            return Result.success("钱包认证成功");
        }else {
            return Result.fail(400,"数据异常");
        }

    }

    @Override
    public Result pay(String token,String paw, Integer money, Integer give) {
        User check = userService.check(token);
        if (check==null){
            return Result.fail(400,"数据异常");
        }
        String md5Pwd = MD5Utils.md5(paw);
        LambdaQueryWrapper<Wallet>queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(Wallet::getUserId,check.getId());
        queryWrapper.eq(Wallet::getWalletPassword,md5Pwd);
        Wallet wallet = walletMapper.selectOne(queryWrapper);
        if (wallet==null){
            return Result.fail(400,"钱包密码错误");
        }
        Integer sum = wallet.getSum();
        Integer total = wallet.getTotal();
        wallet.setSum(sum+money+give);
        wallet.setTotal(total+money);
        int update = walletMapper.update(wallet, queryWrapper);
        if (update==1){
            return Result.success("充值成功");
        }else {
            return Result.fail(400,"充值失败");
        }
    }

    @Override
    public Result info(String token) {
        User check = userService.check(token);
        LambdaQueryWrapper<Wallet> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(Wallet::getUserId,check.getId());
        Wallet wallet = walletMapper.selectOne(queryWrapper);
        if (wallet==null){
            return Result.fail(400,"钱包未认证");
        }else {
            return Result.success(wallet);
        }
    }
}
