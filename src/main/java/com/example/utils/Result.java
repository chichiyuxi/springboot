package com.example.utils;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 返回结果响应
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@ApiModel(value = "Result返回结果对象",description = "封装接口返回给前端的数据")
public class Result {

    //响应给前端的状态
    @ApiModelProperty(value = "响应状态",dataType = "boolean")
    private Boolean success;

    //响应给前端的状态码
    @ApiModelProperty(value = "响应状态码",dataType = "int")
    private Integer code;

    //响应给前端的提示信息
    @ApiModelProperty(value ="响应提示信息")
    private String msg;

    //响应给前端的数据
    @ApiModelProperty(value ="响应数据")
    private Object data;

    //成功返回
    public static Result success(Object data){
        return new Result(true,200,"success",data);
    }
    //失败返回
    public static Result fail(Integer code,String msg){
        return new Result(false,code,msg,null);
    }
}
