package com.example.vo;

import com.example.entity.Category;
import lombok.Data;

import java.util.List;


@Data
public class CategoryVo extends Category {
    //子分类
    private List<CategoryVo> categories;



}
