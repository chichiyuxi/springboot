package com.example.vo;

import lombok.Data;

import java.io.Serializable;
import java.time.LocalDateTime;


@Data
public class CollectVo implements Serializable {

      private Integer collectId;

      private Integer productId;

      private String productName;

      private Integer categoryId;

      private Integer soldNum;

      private Integer productStatus;

      private String mainPic;

      private String content;

      private LocalDateTime createTime;

      private LocalDateTime updateTime;


}
