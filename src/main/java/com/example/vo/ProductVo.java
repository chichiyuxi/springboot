package com.example.vo;

import com.example.entity.Product;
import com.example.entity.ProductImg;
import lombok.Data;

import java.util.List;

@Data
public class ProductVo extends Product {

    private List<ProductImg> imgList;

}
