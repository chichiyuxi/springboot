package com.example.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel(value = "User注册对象", description = "")
public class RegisterVo {

    @ApiModelProperty("用户名")
    private String username;

    @ApiModelProperty("密码")
    private String password;

    @ApiModelProperty("昵称")
    private String nickname;

//    @ApiModelProperty("头像")
//    private MultipartFile userImg;

    @ApiModelProperty("头像")
    private String userImg;
}
