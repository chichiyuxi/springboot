package com.example.vo;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class ResetVo {
    @ApiModelProperty("用户名")
    private String username;

    @ApiModelProperty("老密码")
    private String oldPassword;

    @ApiModelProperty("新密码")
    private String password;

}
