package com.example.vo;

import com.example.entity.ShoppingCart;
import lombok.Data;

@Data
public class ShoppingCartVo extends ShoppingCart {
    private String productName;

    private String productImg;

    private String originalPrice;

    private String sellPrice;

    private String skuName;


}
