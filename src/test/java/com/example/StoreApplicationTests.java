package com.example;

import com.example.mapper.CategoryMapper;
import com.example.vo.CategoryVo;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;


@SpringBootTest
class StoreApplicationTests {

    @Autowired
    private CategoryMapper categoryMapper;

    @Test
    void contextLoads() {
        List<CategoryVo> categoryVos = categoryMapper.selectAllCategories();
        for (CategoryVo c1:categoryVos){
            System.out.println(c1);
            for (CategoryVo c2:c1.getCategories()){
                System.out.println("\t"+c2);
                for (CategoryVo c3:c2.getCategories()){
                    System.out.println("\t\t"+c3);
                }
            }
        }

    }




}
